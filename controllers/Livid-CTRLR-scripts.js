function LividCTRLR() {}

var chan1 = "[Channel1]";
var chan2 = "[Channel2]";
var chan3 = "[Channel3]";
var chan4 = "[Channel4]";
var master = "[Master]";
var playlist = "[Playlist]";
var library = "[Library]";

var debugBool = true;

var deck1HotCueStart = 0x00; //hotcues go up sequentially from here
var deck2HotCueStart = 0x08; //hotcues go up sequentially from here
var deckHotCueStart = {"[Channel1]" : 0x00, "[Channel2]" : 0x08} //hotcues go up sequentially from here

var deckCueHeld = {
	chan1 : false,
	chan2 : false
}

//shift buttons
LividCTRLR.delCueHeld = false;
LividCTRLR.shiftHeld = false;

LividCTRLR.faders = {
    '[Channel1]': { // an object within another object
        'volume': 0x04
    },
    '[Channel2]': {
        'volume': 0x14
    }
}
LividCTRLR.buttons = {
    '[Channel1]': { // an object within another object
        0x10: "cue_cdj",
        0x11: "play",
        0x12: "sync_enabled"
    },
    '[Channel2]': {
        0x18: "cue_cdj",
        0x19: "play",
        0x1A: "sync_enabled"
    }
}
LividCTRLR.colors = {
    'off': 0,
    'white': 1,
    'cyan': 4,
    'magenta': 8,
    'red': 16,
    'blue': 32,
    'yellow': 64,
    'green': 127,
}
LividCTRLR.beatLoops = [0.125, 0.25, 0.5, 1, 0.03125, 0.0625, 2, 4]; //values for the auto-loopers to select from, second 4 are shift-available
LividCTRLR.beatLoopMidiOffset = {"[Channel1]" : 0x04, "[Channel2]" : 0x0C};
LividCTRLR.playlistEncs = [48,51,54,57];
LividCTRLR.scrubEncs = [49,52,55,58];
LividCTRLR.pitchEncs = [50,53,56,59];
LividCTRLR.hotcues = [0,1,2,3,8,9,10,11];
LividCTRLR.loopers = [4,5,6,7,12,13,14,15];
LividCTRLR.cueButts = [0x10,0x18];
LividCTRLR.playButts = [0x11,0x19];
LividCTRLR.scrollButts = [0x16,0x17,0x1E,0x1F];
LividCTRLR.loopButts = [0x22,0x23,0x2A,0x2B];
LividCTRLR.bendButts = [0x14,0x15,0x1C,0x1D];
LividCTRLR.specButts = [0x20,0x21,0x28,0x29];
LividCTRLR.revButts = [0x12,0x1A];
LividCTRLR.headButts = [0x13,0x1B];
LividCTRLR.syncButts = [0x24,0x25,0x26,0x27,0x2C,0x2D,0x2E,0x2F];

LividCTRLR.init = function (id, debug) { // called when the device is opened & set up
	debugBool = debug;
	print("###init-loaded##############");
	print(Date());
	
	if(debugBool){ print("DEBUG IS ON");}
	else{ print("DEBUG IS OFF");}

	//setup cntrl:r
	//set encoders to relative
	var sysexArray = [ 0xF0, 0x00, 0x01, 0x61, 0x08, 0x11, 0x7F, 0x7F, 0x7F, 0xF7 ];
	midi.sendSysexMsg(sysexArray, sysexArray.length);
	//F0 00 01 61 08 10 E1 E2 E3 E4 F7 
	//0xF0 0x00 0x01 0x61 0x08 0x10 0x00 0x00 0x00 0x00 0xF7 

	engine.softTakeover(chan1, "volume", true);
	engine.softTakeover(chan2, "volume", true);

	clearAllLeds();
	//cues - red
	setGroupLeds(LividCTRLR.hotcues,LividCTRLR.colors['off']);
	setHotCueLeds(chan1); 
	setHotCueLeds(chan2);
	
	//samplers - cyan
	setGroupLeds(LividCTRLR.loopers, LividCTRLR.colors['cyan']);

	//playlist - yellow
	setGroupLeds(LividCTRLR.playlistEncs, LividCTRLR.colors['white']);
	
	//scrub - green
	setGroupLeds(LividCTRLR.scrubEncs, LividCTRLR.colors['red']);

	//pitch - blue
	setGroupLeds(LividCTRLR.pitchEncs,LividCTRLR.colors['blue']);
	
	//cue buttons - red
	setGroupLeds(LividCTRLR.cueButts,LividCTRLR.colors['red']);
	//play buttons - yellow
	setGroupLeds(LividCTRLR.playButts,LividCTRLR.colors['yellow']);
	//play buttons - cyan
	setGroupLeds(LividCTRLR.scrollButts,LividCTRLR.colors['green']);
	//play buttons - blue
	setGroupLeds(LividCTRLR.bendButts,LividCTRLR.colors['blue']);
	//shift buttons - white
	setGroupLeds(LividCTRLR.specButts,LividCTRLR.colors['white']);
	//play buttons - blue
	setGroupLeds(LividCTRLR.headButts,LividCTRLR.colors['off']);
	setGroupLeds(LividCTRLR.syncButts,LividCTRLR.colors['yellow']);
	setGroupLeds(LividCTRLR.revButts,LividCTRLR.colors['blue']);
	setGroupLeds(LividCTRLR.loopButts,LividCTRLR.colors['cyan']);
};

clearAllLeds = function(){
	for(i = 0; i < 128; i++){
		midi.sendShortMsg(0x90, i, LividCTRLR.colors['off']);
	}
}
setGroupLeds = function(ingroup, color){
	for( i = 0; i < ingroup.length; i++){
    	midi.sendShortMsg(0x90, ingroup[i], color);
	}
}
myDebug = function(data){
	if(debugBool){
		print(data);
	}
};

setHotCueLeds = function(group){
	hotCueStart = deckHotCueStart[group]; //set starting point for hotcue markers based on channel
	for(i = 0; i < 4; i++){
		if(engine.getValue(group,'hotcue_'+(i+1)+'_enabled'))
		{
    		midi.sendShortMsg(0x90, hotCueStart+i, LividCTRLR.colors['red']);
		}//end if hotcue is on
	}
}

LividCTRLR.shutdown = function () {
	myDebug("###shutdown##############");
};

var safeToAct = function(group){
	return (LividCTRLR.shiftHeld || !engine.getValue(group,"play"));
}

LividCTRLR.volume = function (channel, control, value, status, group) {
	engine.setValue(group, "volume", norml(value));
	myDebug("volume to "+norml(value));
	
};

LividCTRLR.libraryNav = function (channel, control, value, status, group) {
	(midi2bool(value) ? engine.setValue(playlist,"SelectPrevPlaylist",value) : engine.setValue(playlist,"SelectNextPlaylist",value));
	myDebug("SelectPlaylist"+midi2bool(value));
};

LividCTRLR.librarySel = function (channel, control, value, status, group) {
	// (value == 0x40 ? engine.setValue(playlist,"ToggleSelectedSidebarItem",1) : );
	(value == 0x40 ? engine.setValue(playlist,"ToggleSelectedSidebarItem",1):script.noOp);
	myDebug("PlaylistSel");
};

LividCTRLR.playlistSel = function (channel, control, value, status, group) {
	// (value == 0x40 ? engine.setValue(playlist,"ToggleSelectedSidebarItem",1) : );
	if(value == 0x40){
		if(control == 0x33){
			engine.setValue(playlist,"LoadSelectedIntoFirstStopped",1)
		}else{
			engine.setValue(group,"LoadSelectedTrack",1)
		}
	} //end if is noteon
	myDebug("PlaylistSel");
};

LividCTRLR.playlistNav = function (channel, control, value, status, group) {
	if(LividCTRLR.shiftHeld){
		engine.setValue(playlist,"SelectTrackKnob",script.absoluteLin(value,10,-10,1,127));
	}else{
		engine.setValue(playlist,"SelectTrackKnob",script.absoluteLin(value,1,-1,1,127));
	}
	myDebug(script.absoluteLin(value,1,-1,0,127));	
};

LividCTRLR.deckCue = function (channel, control, value, status, group) {
	if(deckCueHeld[group] && value >0x00 ){ //if button is already held and this is not noteOff, then play
		LividCTRLR.deckPlay(channel,control,value,status,group);
	}else{
		engine.setValue(group, "cue_default", midi2bool(value));
		toggleColor(LividCTRLR.colors['white'],LividCTRLR.colors['red'],control,value);
	}
	deckCueHeld[group] = (value == 0x40 ? true : false); //set per-deck flag if cue button down
};

LividCTRLR.deckPlay = function (channel, control, value, status, group) {
	if(value == 0x40){
		engine.setValue(group, "play", !engine.getValue(group,"play"));//engine.getValue(group,"play"));
		toggleColor(LividCTRLR.colors['green'],LividCTRLR.colors['yellow'],control,engine.getValue(group,"play"));
		myDebug("play "+engine.getValue(group,"play"));
	}
};

LividCTRLR.hotCue = function (channel, control, value, status, group) {
	var hotcueInt;
	if(group == chan1){
		hotcueInt = control - (deck1HotCueStart - 1);
	}else{
		hotcueInt = control - (deck2HotCueStart - 1);
	}
	if(LividCTRLR.delCueHeld){
		engine.setValue(group, "hotcue_"+hotcueInt+"_clear", midi2bool(value));
		setColor(LividCTRLR.colors['off'],control);
	}else{
		engine.setValue(group, "hotcue_"+hotcueInt+"_activate", midi2bool(value));
		toggleColor(LividCTRLR.colors['white'],LividCTRLR.colors['red'],control,value);
		// setColor(LividCTRLR.colors['red'],control);
	}
	myDebug(hotcueInt);
};

LividCTRLR.deckSync = function (channel, control, value, status, group) {
	if(value == 0x40){
		engine.setValue(group, "sync_enabled", !engine.getValue(group,"sync_enabled"));
		toggleColor(LividCTRLR.colors['yellow'],LividCTRLR.colors['green'],control,engine.getValue(group,"sync_enabled"));
	}
};

LividCTRLR.beats_translate_curpos = function (channel, control, value, status, group) {
	if(value == 0x40){
		engine.setValue(group, "beats_translate_curpos", 1);
	}
	toggleColor(LividCTRLR.colors['white'],LividCTRLR.colors['yellow'],control,value);
};

LividCTRLR.repeat = function (channel, control, value, status, group) {
	if(value == 0x40){
		engine.setValue(group, "repeat", !engine.getValue(group,"repeat"));
	}
	toggleColor(LividCTRLR.colors['green'],LividCTRLR.colors['yellow'],control,engine.getValue(group,"repeat"));
};
LividCTRLR.quantize = function (channel, control, value, status, group) {
	if(value == 0x40){
		engine.setValue(group, "quantize", !engine.getValue(group,"quantize"));
	}
	toggleColor(LividCTRLR.colors['green'],LividCTRLR.colors['yellow'],control,engine.getValue(group,"quantize"));
};
LividCTRLR.pfl = function (channel, control, value, status, group) {
	if(value == 0x40){
		engine.setValue(group, "pfl", !engine.getValue(group,"pfl"));
	}
	toggleColor(LividCTRLR.colors['red'],LividCTRLR.colors['off'],control,engine.getValue(group,"pfl"));
};
LividCTRLR.reverseroll = function (channel, control, value, status, group) {
	engine.setValue(group, "reverseroll", !engine.getValue(group,"reverseroll"));
	toggleColor(LividCTRLR.colors['white'],LividCTRLR.colors['blue'],control,midi2bool(value));
};

LividCTRLR.beatLoop = function (channel, control, value, status, group) {
	var offset = control - LividCTRLR.beatLoopMidiOffset[group];
	if(LividCTRLR.shiftHeld){ offset += 4.0;}
	if(LividCTRLR.delCueHeld){
		if(value){
			engine.setValue(group, "beatloop_"+LividCTRLR.beatLoops[offset]+"_toggle", midi2bool(value));
			toggleColor(LividCTRLR.colors['green'],LividCTRLR.colors['cyan'],control,engine.getValue(group,"beatloop_"+LividCTRLR.beatLoops[offset]+"_enabled"));	
		}
	}else{
		engine.setValue(group, "beatlooproll_"+LividCTRLR.beatLoops[offset]+"_activate", midi2bool(value));
		toggleColor(LividCTRLR.colors['green'],LividCTRLR.colors['cyan'],control,midi2bool(value));
	}
	myDebug("Beatloop "+ control + " " + LividCTRLR.beatLoops[offset])
};
LividCTRLR.loopMultiply = function(channel, control, value, status, group){
	engine.setValue(group, "loop_double",value);
}
LividCTRLR.loopDivide = function(channel, control, value, status, group){
	engine.setValue(group, "loop_halve",value);
}

LividCTRLR.delHotcue = function (channel, control, value, status, group) {
    // Note that there is no 'if (value)' here so this executes both when the shift button is pressed and when it is released.
    // Therefore, MyController.shift will only be true while the shift button is held down
    LividCTRLR.delCueHeld = (value == 0x00 ? false : true);//! LividCTRLR.delCueHeld // '!' inverts the value of a boolean (true/false) variable 
    if(LividCTRLR.delCueHeld){ setGroupLeds(LividCTRLR.loopers, LividCTRLR.colors['yellow']) }
    else{ setGroupLeds(LividCTRLR.loopers, LividCTRLR.colors['cyan']) }
	for( i = 0; i < LividCTRLR.beatLoops.length; i++){
		if (engine.getValue(chan1,"beatloop_"+LividCTRLR.beatLoops[i]+"_enabled")){
	    	midi.sendShortMsg(0x90, LividCTRLR.loopers[i], LividCTRLR.colors['green'] );
	    	myDebug(i);
		}
	}
    myDebug("delCursor is "+LividCTRLR.delCueHeld);
}
LividCTRLR.shiftButton = function (channel, control, value, status, group) {
    // Note that there is no 'if (value)' here so this executes both when the shift button is pressed and when it is released.
    // Therefore, MyController.shift will only be true while the shift button is held down
    LividCTRLR.shiftHeld = ! LividCTRLR.shiftHeld // '!' inverts the value of a boolean (true/false) variable 
    myDebug("Shift is "+LividCTRLR.shiftHeld);
}

LividCTRLR.pitch = function (channel, control, value, status, group) {
	adjustPitch(group,value,0.01);
};
LividCTRLR.pitchFine = function (channel, control, value, status, group) {
	adjustPitch(group,value,0.0003);
};
LividCTRLR.pitchReset = function (channel, control, value, status, group) {
	engine.setValue(group,"rate",0);
};

LividCTRLR.scrollCoarse = function (channel, control, value, status, group) {
	if(safeToAct(group)){
		if(value > 0x01){
			adjustPosition(group,1,0.01);
		}else{	
			adjustPosition(group,127,0.01);
		}
	}
};
LividCTRLR.scrollFine = function (channel, control, value, status, group) {
	if(safeToAct(group)){
		if(value > 0x01){
			LividCTRLR.scrollFineLeft(channel,control,value,status,group);
		}else{	
			LividCTRLR.scrollFineRight(channel,control,value,status,group);	
		}
	}//if not playing or if shift key is held
};
LividCTRLR.scrollFineLeft = function (channel, control, value, status, group) {
	if(value > 0x00){
		adjustPosition(group,1,0.0001);
	}
	toggleColor(LividCTRLR.colors['blue'],LividCTRLR.colors['white'],control,value);
};
LividCTRLR.scrollFineRight = function (channel, control, value, status, group) {
	if(value > 0x00){
		adjustPosition(group,127,0.0001);
	}
	toggleColor(LividCTRLR.colors['blue'],LividCTRLR.colors['white'],control,value);
};

LividCTRLR.scrub = function (channel, control, value, status, group) {
	if(safeToAct(group)){
		engine.setValue(group,"playposition",norml(value));
	}
};

adjustPitch = function (group,value,multiplier){
	var modVal = script.absoluteLin(value,-1,1,1,127);
	var newVal = engine.getValue(group,"rate") + modVal * multiplier;
	myDebug("pitch "+engine.getValue(group,"rate"));
	engine.setValue(group,"rate",newVal);
}

adjustPosition = function (group,value,multiplier){
	var modVal = script.absoluteLin(value,-1,1,1,127);
	var newVal = engine.getValue(group,"playposition") + modVal * multiplier;
	myDebug("position "+engine.getValue(group,"playposition"));
	engine.setValue(group,"playposition",newVal);
}

turnOffHotcues = function(group){
	if(LividCTRLR.hotcuePlaying[group]){ 
		for( i = 1; i <= 4; i++){
			engine.setValue(group,"hotcue_"+i+"_activate",false);
			LividCTRLR.hotcuePlaying[group] = false;
			myDebug(i);
		}
	}//if already playing
};

norml = function(value){
	// return script.absoluteLin(value,0,1,0,127);
	return value/127;
};
midi2bool = function(value){
	// return script.absoluteLin(value,0,1,0,127);
	return (value == 0x00 || value == 0x01 ? false : true);
};

setColor = function(color1,control){
	midi.sendShortMsg(0x90, control, color1);
}

toggleColor = function(color1,color2,control,value){
	if(value){
		midi.sendShortMsg(0x90, control, color1);
	}else{
		midi.sendShortMsg(0x90, control, color2);
	}
}