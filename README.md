# README #

Install these two files into the respective folders in:
/Users/*YOU*/Library/Application Support/Mixxx/

It will look like:
/Users/*YOU*/Library/Application Support/Mixxx/presets/Livid-CTRLR.midi.xml
and 
/Users/*YOU*/Library/Application Support/Mixxx/controllers/Livid-CTRLR-scripts.js

WORKS WITH CNTRL:R DEFAULT MAPPING

### What is this repository for? ###

Provides a featured controller mapping for the Livid Instruments CTRL:R midi controller. Sync, manual adjustment of bpm, volume, eq, fx, loops, cues, etc are all supported.
Some controls are purposefully doubled to allow ease of access in different areas of the controller.
